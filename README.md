# Tezos CI scripts

This folder contains a bunch of scripts for the
[tezos/tezos](https://gitlab.com/tezos/tezos) project. Most of them
will have little use for other people than me, but here are some
instructions anyhow.

## Installation

These scripts are in a subfolder of a fork of the
[ocaml-gitlab](https://github.com/tmcgilchrist/ocaml-gitlab/)
project. This makes it easier to upstream patches I make to that
library.

To just use the scripts, the general procedure would be:

 - create a switch
 - install the `ocaml-gitlab` dependencies
 - do `dune build`

I don't think there are any other dependencies to install. Sorry for
the poor installation instructions, if you can help me improve them or
improve the layout of this repo, do not hesitate.

## Overview

Here are the scripts that might have some use for other peoples:

 - `testing_effort`: Used to generate the agenda for the testing effort
 - `retried_tests`: Detects flaky tests by looking for retried jobs
 - `diagnose`: Dignose the failures in a given job
 - `mtc_metrics`: Merge team coordination metrics
 - `sections`: Printing the timing of a specific job

## Usage

### `mtc_metrics`

This script produces merge team metrics. Currently, it only gives
information on the merge time and size of merge requests. It requires
a local checkout of `tezos/tezos` (whose path is given using
`--local-repo`) to give the number of changes in the MR.

Example invocation:
```
$ dune exec ./mtc_metrics.exe -- --private-token "glpat-..." --created-after "2022-06-28\ 19:29:43+02:00" --created-before "2022-07-05\ 19:29:43+02:00" --limit 10 --local-repo ~/dev/nomadic-labs/tezos/master
```

Example output:
```
MR metrics for merge requests in project 3836952, got 5 MRs  [limit=5, created_after=2022-06-28\ 19:29:43+02:00, created_before=2022-07-05\ 19:29:43+02:00]
MR iid,MR title,MR state,MR additions,MR deletions,MR changes,MR merge time (seconds),MR merge time (days)
!5784,Scoru: various fixes and improvements,'merged',155,200,355,197526.108,2.28618180556
!5782,ORU: rollup nodes communicate in binary with Tezos node by default,'merged',5,5,10,153341.902,1.77479053241
!5781,Fix snoop failing when given benchmark config with wrong directory,'merged',201,32,233,513676.667,5.94533179398
!5777,Backport !5731 -  Proto/Events: EMIT types,'merged',88,54,142,40317.674,0.466639745371
!5775,Backport !5715 - Proto: event emission is not a transaction,'merged',181,206,387,39903.0710001,0.461841099538
```
