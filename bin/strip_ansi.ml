let read_line_opt () = try Some (read_line ()) with End_of_file -> None
let escape s = String.concat "^[" @@ String.split_on_char '\027' s

let strip s =
  let rec aux state buf s =
    match s with
    | [] -> []
    | c :: s' -> (
        match (state, c) with
        | `Init, '\027' -> aux `Fst (c :: buf) s'
        | `Init, c ->
            assert (List.length buf = 0);
            c :: aux `Fst [] s'
        | `Fst, '[' -> aux `Dig (c :: buf) s'
        | `Fst, c -> List.rev buf @ aux `Init [] (c :: s')
        | `Dig, ('0' .. '9' | ';') -> aux `Dig (c :: buf) s'
        | `Dig, ('m' | 'K') -> aux `Init [] s'
        | `Dig, c -> List.rev buf @ aux `Init [] (c :: s'))
  in
  aux `Init [] (String.to_seq s |> List.of_seq) |> List.to_seq |> String.of_seq

let () =
  let rec aux () =
    match read_line_opt () with
    | Some line ->
        (*         Format.printf "'%s' -> '%s'\n\n" (escape line) (strip line); *)
        Format.printf "'%s'\n\n" (strip line);
        aux ()
    | None ->
        (*             Format.printf "< End\n"; *)
        ()
  in
  aux ()
