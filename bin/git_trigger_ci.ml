let branch_in_dir dir = Git.branch_of_repository dir
let project_of_remote _remote = assert false

let mr_of_branch _remote _branch =
  (*   let open Gitlab.Monad in *)
  (*   Gitlab.Project.merge_requests *)
  assert false

let latest_pipeline_of_branch_of_mr _mr = assert false
let trigger_pipeline = assert false
let mr_in_dir () = assert false

(* let () = *)
(*   Lwt_main.run *)
(*   @@ Gitlab.Monad.( *)
(*        run ( *)
