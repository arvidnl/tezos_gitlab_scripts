open Util

(*

 Get all pipelines that marge triggered that failed.

   For each pipeline, compare the pipeline termination time with
   termination time of the last job and termination job of the first
   failed job.

*)
let per_page = 20
let username = "nomadic-margebot"

let pl_of_single
    ({
       id;
       iid;
       project_id;
       status;
       source;
       ref;
       sha;
       web_url;
       created_at;
       updated_at;
       _;
     } :
      Gitlab_t.single_pipeline) : Gitlab_t.pipeline =
  let created_at = Option.get created_at in
  let updated_at = Option.get updated_at in
  {
    id;
    iid;
    project_id;
    status;
    source;
    ref;
    sha;
    web_url;
    created_at;
    updated_at;
  }

let get_early_fails ~token ~project_id pipeline_id =
  let open Gitlab in
  let open Monad in
  let* marges_pipelines =
    return @@ Gitlab.Project.pipelines ~token ~project_id ~per_page ~username ()
  in
  let* () =
    pft
      "id,ref,status,created_at,finished_at,min_termination_time,time_saved,pl \
       web_url, job web_url\n"
  in

  let handle_pipeline (pl : Gitlab_t.pipeline) =
    let* pl_single =
      Gitlab.Project.pipeline ~token ~project_id ~pipeline_id:pl.id ()
    in
    let pl_single = Gitlab.Response.value pl_single in

    let* min_termination_job =
      match pl.status with
      | `Failed ->
          let* failed_jobs =
            return
            @@ Gitlab.Project.pipeline_jobs ~token ~project_id ~per_page
                 ~pipeline_id:pl.id ~scope:`Failed ~include_retried:false ()
          in

          Stream.fold
            (fun min_termination_job (job : Gitlab_t.pipeline_job) ->
              let min_termination_time =
                Option.bind min_termination_job @@ fun job ->
                job.Gitlab_t.finished_at
              in
              return
              @@
              match (min_termination_time, job.finished_at) with
              | None, Some _ -> Some job
              | Some t1, Some t2 when t2 < t1 -> Some job
              | Some _, _ -> min_termination_job
              | None, None -> None)
            None failed_jobs
      | _ -> return None
    in

    let min_termination_time =
      Option.bind min_termination_job @@ fun job -> job.Gitlab_t.finished_at
    in

    let time_saved =
      Option.bind min_termination_time @@ fun min_termination_time ->
      Option.bind pl_single.finished_at @@ fun finished_at ->
      Some (finished_at -. min_termination_time)
    in

    pft "#%d,%s,%s,%s,%s,%s,%.2f min,%s, %s\n" pl.id pl.ref
      (Gitlab_j.string_of_pipeline_status pl.status)
      (ISO8601.Permissive.string_of_datetime pl.created_at)
      (Option.fold ~none:"" ~some:ISO8601.Permissive.string_of_datetime
         pl_single.finished_at)
      (Option.fold ~none:"" ~some:ISO8601.Permissive.string_of_datetime
         min_termination_time)
      (Option.value ~default:0.0 time_saved /. 60.0)
      pl.web_url
      (Option.fold ~none:""
         ~some:(fun (job : Gitlab_t.pipeline_job) ->
           sf "%s (%s)" job.web_url job.name)
         min_termination_job)
  in
  match pipeline_id with
  | Some pipeline_id ->
      let* pl_single =
        Gitlab.Project.pipeline ~token ~project_id ~pipeline_id ()
      in
      let pl_single = Gitlab.Response.value pl_single in
      let pl = pl_of_single pl_single in
      handle_pipeline pl
  | None -> Stream.iter handle_pipeline marges_pipelines

let () =
  (* tezos/tezos *)
  let project_id = ref None in
  let token = ref None in
  let pipeline_id = ref None in
  let args_spec =
    [
      ( "--private-token",
        Arg.String (fun t -> token := Some (Gitlab.Token.of_string t)),
        "<TOKEN> GitLab private token." );
      ( "--project-id",
        Arg.Int (fun i -> project_id := Some i),
        "<PROJECT_ID> Numeric GitLab project id." );
      ( "--pipeline-id",
        Arg.Int (fun i -> pipeline_id := Some i),
        "<ID> Look for flaky jobs in this pipeline only. Useful for debugging."
      )
      (* ( "--format",
       *   Arg.String
       *     (function
       *     | "md" | "markdown" -> format := Markdown
       *     | "ascii" -> format := Ascii
       *     | "json" -> format := JSON
       *     | _ ->
       *         raise
       *         @@ Invalid_argument
       *              "--format must be either 'md', 'markdown', 'json' or 'ascii'"),
       *   " Format. Can be either 'markdown', 'md', 'json' or 'ascii'." ); *);
    ]
  in
  let usage_msg =
    Printf.sprintf "Usage: %s [options]\nOptions are:" Sys.argv.(0)
  in
  Arg.parse args_spec
    (fun s -> raise @@ Invalid_argument (sf "No anonymous arguments: %s" s))
    usage_msg;
  let token, project_id =
    parse_config ~default_project_id:!project_id ~default_token:!token
  in
  Lwt_main.run
  @@ Gitlab.Monad.(run (get_early_fails ~token ~project_id !pipeline_id))
