#!/bin/bash

if [ -n "${TRACE:-}" ]; then set -x; fi

set -eu

LIMIT=${LIMIT:-500}

outfile=retried_tests_"$(date +%Y-%m-%d)"
outfile="$outfile.log"

echo "Output in $outfile"
if [ -f _build/default/bin/retried_tests.exe ]; then
    cmd=_build/default/bin/retried_tests.exe;
else
    cmd="dune exec ./bin/retried_tests.exe --"
fi
PRIVATE_TOKEN=${PRIVATE_TOKEN:-} $cmd \
             --limit "$LIMIT" \
             --failure-reason script_failure \
             $(if [ -n "${GLUSERNAME:-}" ]; then echo "--username $GLUSERNAME"; fi) \
             "$@" \
    | tee "$outfile"
