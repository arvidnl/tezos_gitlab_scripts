set style data histograms
set style histogram rowstacked
set boxwidth 1 relative
set style fill solid 1.0 border -1
set xtics rotate by 45 offset -0,-1
set yrange [0:1000]
set datafile separator ","
plot 'build.csv' using 2 t "archive cache", '' using 3 t "cleanup file variables", '' using 4 t "get sources", '' using 5 t "prepare executor", '' using 6 t "prepare script", '' using 7 t "resolve secrets", '' using 8 t "restore cache", '' using 9 t "step script", '' using 10:xticlabels(1) t "upload artifacts on success"
# plot 'build.csv'
# plot 'my.dat' using 2 t "March", '' using 3 t "April", '' using 4:xtic(1) t "May"
