#!/bin/bash

set -eu

date=$1
date_end=$2
step=${3:-1 days}
limit=${LIMIT:-100}
output_dir="retried_tests_history_$date--$date_end"
mkdir -p "$output_dir"

dune build ./retried_tests.exe

while true; do
    date_next=$(date --date="$date + $step" --rfc-3339 seconds)

    outfile="$output_dir"/retried_tests_history_"$(date -d "$date" +%Y-%m-%d)".log
    echo "Output in $outfile"
    ../_build/default/tezos-scripts/retried_tests.exe \
         --private-token  "$(keyring get 'gitlab-api' 'arvidnl/python-gitlab_api')" \
         --limit "$limit" \
         --failure-reason script_failure \
         --updated-after "$date" \
         --updated-before "$date_next" \
         --order-by id \
         --sort asc \
        > "$outfile"

    date=$date_next
    date_s=$(date -d "$date" +%s)
    date_end_s=$(date -d "$date_end" +%s)
    if [ "$date_s" -gt "$date_end_s" ]; then
       break;
    fi
done
