#!/bin/bash

dune exec bin/sections.exe -- --private-token  "$(keyring get 'gitlab-api' 'arvidnl/python-gitlab_api')" "$@"
