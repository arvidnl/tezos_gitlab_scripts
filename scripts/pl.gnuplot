set key autotitle columnhead

set xdata time
set timefmt "%Y-%m-%d"
set xrange ["2022-05-01":"2022-06-10"]
set format x "%m-%d"
set datafile separator ","

set boxwidth 4
set style fill solid
plot "data.csv" using 1:2 with boxes
