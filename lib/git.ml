open Util

type numstat = { additions : int; deletions : int }

let git_cmd repo args = ("git", [ "-C"; repo ] @ args)

let numstat ~repository ~commit1 ~commit2 =
  let cmd =
    git_cmd repository [ "diff"; sf "%s..%s" commit1 commit2; "--numstat" ]
  in
  let additions, deletions =
    List.fold_left
      (fun (additions, deletions) line ->
        match String.split_on_char '\t' line with
        | [ additions'; deletions'; _path ] ->
            ( additions + int_of_string additions',
              deletions + int_of_string deletions' )
        | _ -> failwith "Unexpected ")
      (0, 0)
      (Proc.cmd_to_list_and_check cmd)
  in
  { additions; deletions }

let commit_parents ~repository ~commit =
  Proc.cmd_to_list_and_check
    (git_cmd repository [ "rev-parse"; sf "%s^@" commit ])

let branch_of_repository repository =
  Proc.cmd_to_list_and_check
    (git_cmd repository [ "rev-parse"; "--abbrev-ref"; "HEAD" ])

let push_remote_of_repository repository branch =
  Proc.cmd_to_list_and_check
    (git_cmd repository
       [ "config"; "--get"; "branch." ^ branch ^ ".pushRemote" ])
