type t = string * Re.re

let compile ?opts r = (r, Re.compile (Re.Perl.re ?opts r))

let pp : Format.formatter -> t -> unit =
 fun fmt (r, _) -> Format.fprintf fmt "/%s/" r

let ( =~ ) s (_, r) = Re.execp r s
let ( =~! ) s (_, r) = not (Re.execp r s)

let get_group group index =
  match Re.Group.get group index with
  | exception Not_found ->
      invalid_arg
        "regular expression has not enough capture groups for its usage, did \
         you forget parentheses?"
  | value -> value

let ( =~* ) s (_, r) =
  match Re.exec_opt r s with
  | None -> None
  | Some group -> Some (get_group group 1)

let ( =~** ) s (_, r) =
  match Re.exec_opt r s with
  | None -> None
  | Some group -> Some (get_group group 1, get_group group 2)

let ( =~*** ) s (_, r) =
  match Re.exec_opt r s with
  | None -> None
  | Some group -> Some (get_group group 1, get_group group 2, get_group group 3)

let ( =~^ ) s (_, r) =
  match Re.exec_opt r s with
  | None -> None
  | Some group -> Some (Re.Group.all group)
